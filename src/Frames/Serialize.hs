{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE CPP #-}
{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DerivingVia         #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -fwarn-incomplete-patterns #-}
module Frames.Serialize
  (
    -- * Newtype for instances
    SElField(..)
    -- * Constraint names
  , RecSerialize
  , RecBinary
  , RecFlat
    -- * Record coercions
  , toS
  , fromS
  -- * Wrapper for serializable frames
  , SFrame (..)
  , SFrameRec
  , SFrameA (..)
  , SFrameRecA
{-
  , SFrameB (..)
  , SFrameRecB
  , SFrameC (..)
  , SFrameRecC
-}
  -- * SFrame to/from bytestring
  , sframeToByteString
  , sframeFromByteString
  )
where

import qualified Control.Foldl as FL
import qualified Control.Monad.ST as ST
import qualified Data.Binary as B
import qualified Data.Binary.Put as B
import qualified Data.Binary.Get as B

import qualified Data.Serialize                as S
import qualified Data.Vinyl                    as V
import qualified Data.Vinyl.TypeLevel          as V
import Data.Vinyl.Core ()

import qualified Flat
import qualified Flat.Decoder.Types as FlatD
import qualified Flat.Encoder.Strict as Flat
import qualified Flat.Encoder.Size as Flat
import qualified Flat.Stream as FlatS

import qualified Frames as F
import qualified Frames.InCore as FI
import qualified Frames.Streamly.InCore as FS
import Frames.Streamly.Streaming.Streamly (StreamlyStream(..))
import           GHC.TypeLits (KnownSymbol)
import           GHC.Generics (Rep, Generic(..))


#if MIN_VERSION_streamly(0,9,0)
import qualified Streamly.Data.Stream as Streamly
import qualified Streamly.Data.StreamK as StreamK
import qualified Streamly.Data.Fold as Streamly.Fold
import qualified Streamly.Data.Unfold as Streamly.Unfold
import qualified Streamly.Internal.Data.Unfold as Streamly.Unfold
import qualified Streamly.Internal.Data.Stream as Streamly.Step -- .StreamD.Step as Streamly.Step
#elif MIN_VERSION_streamly(0,8,0)
import qualified Streamly.Prelude as Streamly
import qualified Streamly.Internal.Data.Unfold.Type as Streamly.Unfold
import qualified Streamly.Data.Fold.Tee as Streamly.Tee
import qualified Streamly.Internal.Data.Fold as Streamly.Fold
import qualified Streamly.Internal.Data.Unfold as Streamly.Unfold
#else
import qualified Streamly.Prelude as Streamly
import qualified Streamly.Internal.Data.Fold as Streamly.Fold
import qualified Streamly.Internal.Data.Unfold as Streamly.Unfold
import qualified Streamly
import qualified Streamly.Internal.Data.Unfold.Types as Streamly.Unfold
import qualified Streamly.Internal.Data.Stream.StreamD.Type as Streamly.Unfold (Step(..))
#endif


import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BL
import qualified ByteString.StrictBuilder as BSB


#if MIN_VERSION_streamly(0,9,0)
type Stream = Streamly.Stream
#else
type Stream = Streamly.SerialT
#endif

newtype SElField t = SElField { unSElField :: V.ElField t }

deriving via (V.ElField '(s,a)) instance (KnownSymbol s, Show a) => Show (SElField '(s,a))
deriving via (V.ElField '(s,a)) instance (KnownSymbol s) => Generic (SElField '(s,a))
deriving via (V.ElField '(s,a)) instance Eq a => Eq (SElField '(s,a))
deriving via (V.ElField '(s,a)) instance Ord a => Ord (SElField '(s,a))

instance (S.Serialize (V.Snd t), V.KnownField t) => S.Serialize (SElField t)
instance (B.Binary (V.Snd t), V.KnownField t) => B.Binary (SElField t)
instance (Flat.Flat (V.Snd t), V.KnownField t) => Flat.Flat (SElField t)

type StreamType = StreamlyStream Stream

toS :: V.RMap rs => V.Rec V.ElField rs -> V.Rec SElField rs
toS = V.rmap coerce
{-# INLINE toS #-}

fromS :: V.RMap rs => V.Rec SElField rs -> V.Rec V.ElField rs
fromS = V.rmap coerce
{-# INLINE fromS #-}

newtype SFrame a = SFrame { unSFrame :: F.Frame a } deriving (Eq)
type SFrameRec rs = SFrame (F.Record rs)

deriving instance Show (F.Frame a) => Show (SFrame a)

type RecSerialize rs = (S.GSerializePut (Rep (V.Rec SElField rs))
                       , S.GSerializeGet (Rep (V.Rec SElField rs))
                       , Generic (V.Rec SElField rs))

instance RecSerialize rs => S.Serialize (V.Rec SElField rs)

encodeOneCereal :: S.Serialize a => a -> BS.ByteString
encodeOneCereal = S.runPut . S.put
{-# INLINE encodeOneCereal #-}

decodeOneCereal :: S.Serialize a => BS.ByteString -> Either Text (a, BS.ByteString)
decodeOneCereal bs = first toText $ S.runGetState S.get bs 0
{-# INLINE decodeOneCereal #-}

instance (V.RMap rs, FI.RecVec rs, RecSerialize rs) => S.Serialize (SFrameRec rs) where
  put = S.putByteString . sframeToByteString @S.Serialize encodeOneCereal
  {-# INLINEABLE put #-}
  get = do
    nRecs <- S.getWord64be
    sfNumBytes <- S.getWord64be
    sfBS <- S.getBytes (fromIntegral sfNumBytes)
    case sframeFromByteString @S.Serialize decodeOneCereal nRecs sfBS of
      Left msg -> fail $ toString msg
      Right sf -> return sf
  {-# INLINEABLE get #-}

type RecBinary rs = (B.GBinaryPut (Rep (V.Rec SElField rs))
                    , B.GBinaryGet (Rep (V.Rec SElField rs))
                    , Generic (V.Rec SElField rs))

instance RecBinary rs => B.Binary (V.Rec SElField rs)

encodeOneBinary :: B.Binary a => a -> BS.ByteString
encodeOneBinary = BL.toStrict . B.runPut . B.put
{-# INLINE encodeOneBinary #-}

decodeOneBinary :: B.Binary a => BS.ByteString -> Either Text (a, BS.ByteString)
decodeOneBinary bs = case B.runGetOrFail B.get (BL.fromStrict bs) of
  Left (_, _, errMsg) -> Left $ toText errMsg
  Right (bs', _, a) -> Right (a, BL.toStrict bs')
{-# INLINE decodeOneBinary #-}

instance (V.RMap rs, FI.RecVec rs, RecBinary rs) => B.Binary (SFrameRec rs) where
  put = B.putByteString . sframeToByteString @B.Binary encodeOneBinary
  {-# INLINEABLE put #-}
  get = do
    nRecs <- B.getWord64be
    sfNumBytes <- B.getWord64be
    sfBS <- B.getByteString (fromIntegral sfNumBytes)
    case sframeFromByteString @B.Binary decodeOneBinary nRecs sfBS of
      Left msg -> fail $ toString msg
      Right sf -> return sf
  {-# INLINEABLE get #-}

type RecFlat rs = (Flat.GFlatDecode (Rep (F.Rec SElField rs))
                  , Flat.GFlatEncode (Rep (F.Rec SElField rs))
                  , Flat.GFlatSize (Rep (F.Rec SElField rs))
                  , Generic (F.Rec SElField rs)
                  )

instance RecFlat rs => Flat.Flat (F.Rec SElField rs)

instance (V.RMap rs, FI.RecVec rs, RecFlat rs, e ~ FlatD.DecodeException) => Flat.Flat (SFrameRec rs) where
  size l = Flat.size (sFrameRecToList unSFrame l)
  encode l = Flat.encode (sFrameRecToList unSFrame l)
  decode = SFrame <$> FlatS.getListViaStreamlyFold (Streamly.Fold.lmap fromS (FS.inCoreAoS_F @rs @StreamType)) Flat.decode

streamlyMap :: Monad m => (a -> b) -> Stream m a -> Stream m b
#if MIN_VERSION_streamly(0,9,0)
streamlyMap = fmap
#else
streamlyMap = Streamly.map
#endif
{-# INLINE streamlyMap #-}

#if MIN_VERSION_streamly(0,9,0)
sFrameRecToList :: V.RMap rs => (a -> F.FrameRec rs) -> a -> [F.Rec SElField rs]
sFrameRecToList unS = runIdentity . Streamly.toList @Identity . streamlyMap toS . (StreamK.toStream . StreamK.fromFoldable) . unS
#else
sFrameRecToList :: V.RMap rs => (a -> F.FrameRec rs) -> a -> [F.Rec SElField rs]
sFrameRecToList unS = runIdentity . Streamly.toList @Identity . streamlyMap toS . Streamly.fromFoldable . unS
#endif

newtype SFrameA a = SFrameA { unSFrameA :: F.Frame a } deriving (Eq)
type SFrameRecA rs = SFrameA (F.Record rs)

instance (V.RMap rs, FI.RecVec rs, RecFlat rs, e ~ FlatD.DecodeException) => Flat.Flat (SFrameRecA rs) where
  size l n =
    let fld = (,) <$> FL.length <*> FL.premap (\x -> Flat.size (toS x) 0) FL.sum
        (nItems, itemBits) = FL.fold fld $ unSFrameA l
    in n + itemBits + Flat.arrayBits nItems
  encode l = Flat.encodeArrayWith Flat.encode (sFrameRecToList unSFrameA l)
  decode = SFrameA <$> FlatS.getArrayViaStreamlyFold (Streamly.Fold.lmap fromS (FS.inCoreAoS_F @rs @StreamType)) Flat.decode


-- we use only one fold to generate number of records, total bytes in serialized records and bytestring itself.
#if MIN_VERSION_streamly(0,9,0)
sframeEncodeComponents :: forall c rs. (c (V.Rec SElField rs), c Word64, V.RMap rs)
                       => (forall a. c a => a -> BS.ByteString) -> SFrameRec rs -> (Word64, Word64, BSB.Builder)
sframeEncodeComponents encodeOne sf =
  let bldOne :: c x => x -> (Int, BSB.Builder)
      bldOne !x = let b = encodeOne x in (BS.length b, BSB.bytes b)
      s :: Stream Identity (Int, BSB.Builder) = fmap (bldOne . toS) $ StreamK.toStream . StreamK.fromFoldable $ unSFrame sf
      fld = Streamly.Fold.unTee
            $ (,,)
            <$> Streamly.Fold.Tee (fmap fromIntegral Streamly.Fold.length)
            <*> Streamly.Fold.Tee (fmap fromIntegral (Streamly.Fold.lmap fst Streamly.Fold.sum))
            <*> Streamly.Fold.Tee (Streamly.Fold.lmap snd Streamly.Fold.mconcat)
  in runIdentity $ Streamly.fold fld s
#elif MIN_VERSION_streamly(0,8,0)
sframeEncodeComponents :: forall c rs. (c (V.Rec SElField rs), c Word64, V.RMap rs) => (forall a. c a => a -> BS.ByteString) -> SFrameRec rs -> (Word64, Word64, BSB.Builder)
sframeEncodeComponents encodeOne sf =
  let bldOne :: c x => x -> (Int, BSB.Builder)
      bldOne !x = let b = encodeOne x in (BS.length b, BSB.bytes b)
      s :: Stream Identity (Int, BSB.Builder) = Streamly.map (bldOne . toS) $ Streamly.fromFoldable $ unSFrame sf
      fld = Streamly.Tee.toFold
            $ (,,)
            <$> Streamly.Tee.Tee (fmap fromIntegral Streamly.Fold.length)
            <*> Streamly.Tee.Tee (fmap fromIntegral (Streamly.Fold.lmap fst Streamly.Fold.sum))
            <*> Streamly.Tee.Tee (Streamly.Fold.lmap snd Streamly.Fold.mconcat)
  in runIdentity $ Streamly.fold fld s
#else
sframeEncodeComponents :: forall c rs. (c (V.Rec SElField rs), c Word64, V.RMap rs) => (forall a. c a => a -> BS.ByteString) -> SFrameRec rs -> (Word64, Word64, BSB.Builder)
sframeEncodeComponents encodeOne sf =
  let bldOne :: c x => x -> (Int, BSB.Builder)
      bldOne !x = let b = encodeOne x in (BS.length b, BSB.bytes b)
      s :: Stream Identity (Int, BSB.Builder) = Streamly.map (bldOne . toS) $ Streamly.fromFoldable $ unSFrame sf
      fld = (,,)
            <$> fmap fromIntegral Streamly.Fold.length
            <*> fmap fromIntegral (Streamly.Fold.lmap fst Streamly.Fold.sum)
            <*> Streamly.Fold.lmap snd Streamly.Fold.mconcat
  in runIdentity $ Streamly.fold fld s
#endif
{-# INLINE sframeEncodeComponents #-}

sframeToByteString :: forall c rs. (c (V.Rec SElField rs), c Word64, V.RMap rs) => (forall a. c a => a -> BS.ByteString) -> SFrameRec rs -> BS.ByteString
sframeToByteString encodeOne sf =
  let (numRecs, numBytes, bldr) = sframeEncodeComponents @c encodeOne sf
      bldWord64 :: forall a. Integral a => a -> BSB.Builder
      bldWord64 = BSB.bytes . encodeOne . fromIntegral @a @Word64
      bldrWithPrefix = bldWord64 numRecs <> bldWord64 numBytes <> bldr
  in BSB.builderBytes bldrWithPrefix
{-# INLINEABLE sframeToByteString #-}

data UEState a = Go !a | Stop

#if MIN_VERSION_streamly(0,9,0)
unfoldEither :: forall m a c d e. Monad m => (a -> m c) -> (c -> m (Either e (d, c))) -> (c -> Bool) -> Streamly.Unfold.Unfold m a (Either e d)
unfoldEither inject eStep stopIf = Streamly.Unfold.Unfold step inject' where
  inject' :: a -> m (UEState c)
  inject' = fmap Go . inject
  step :: UEState c -> m (Streamly.Step.Step (UEState c) (Either e d))
  step us = case us of
    Stop -> return Streamly.Step.Stop
    Go c -> if stopIf c
            then return Streamly.Step.Stop
            else eStep c >>= \case
      Left e -> return $ Streamly.Step.Yield (Left e) Stop
      Right (d, c') -> return $ Streamly.Step.Yield (Right d) (Go c')
#else
unfoldEither :: forall m a c d e. Monad m => (a -> m c) -> (c -> m (Either e (d, c))) -> (c -> Bool) -> Streamly.Unfold.Unfold m a (Either e d)
unfoldEither inject eStep stopIf = Streamly.Unfold.Unfold step inject' where
  inject' :: a -> m (UEState c)
  inject' = fmap Go . inject
  step :: UEState c -> m (Streamly.Unfold.Step (UEState c) (Either e d))
  step us = case us of
    Stop -> return Streamly.Unfold.Stop
    Go c -> if stopIf c
            then return Streamly.Unfold.Stop
            else eStep c >>= \case
      Left e -> return $ Streamly.Unfold.Yield (Left e) Stop
      Right (d, c') -> return $ Streamly.Unfold.Yield (Right d) (Go c')
#endif

data Seed = Seed !Word64 !BS.ByteString

uDeserialize ::  forall c rs s. (c (V.Rec SElField rs), V.RMap rs)
              => (forall a. c a => BS.ByteString -> Either Text (a, BS.ByteString))
              -> Word64
              -> Streamly.Unfold.Unfold (ST.ST s) Seed (Either Text (F.Rec F.ElField rs))
uDeserialize decodeOne n = unfoldEither return eStep stopIf where
  eStep :: forall s1. Seed -> ST.ST s1 (Either Text (F.Rec F.ElField rs, Seed))
  eStep (Seed k bs) = return $ bimap fromS (Seed (k+1)) <$> decodeOne bs
  stopIf (Seed k _) = k >= n
{-# INLINE uDeserialize #-}

#if MIN_VERSION_streamly(0,9,0)
sframeFromByteString :: forall c rs. (FS.RecVec rs, V.RMap rs, c Word64, c (F.Rec SElField rs))
                     => (forall a. c a => BS.ByteString -> Either Text (a, BS.ByteString)) -> Word64 -> BS.ByteString -> Either Text (SFrameRec rs)
sframeFromByteString decodeOne n bs =
  let fToFrame :: forall s. Streamly.Fold.Fold (ST.ST s) (Either Text (F.Rec F.ElField rs)) (F.FrameRec rs)
      fToFrame = Streamly.Fold.lmap rightToMaybe $ Streamly.Fold.catMaybes (FS.inCoreAoS_F @rs @StreamType)
      fToErrs :: forall s. Streamly.Fold.Fold (ST.ST s) (Either Text (F.Rec F.ElField rs)) [Text]
      fToErrs = Streamly.Fold.lmap leftToMaybe $ Streamly.Fold.catMaybes Streamly.Fold.toList
      fld :: forall s. Streamly.Fold.Fold (ST.ST s) (Either Text (F.Rec F.ElField rs)) (F.FrameRec rs, [Text])
      fld = Streamly.Fold.unTee $ (,) <$> Streamly.Fold.Tee fToFrame <*> Streamly.Fold.Tee fToErrs
      (frame, errs) = ST.runST $ Streamly.fold fld $ Streamly.unfold (uDeserialize @c decodeOne n) (Seed 0 bs)
        --Streamly.Unfold.fold fld (uDeserialize @c decodeOne n) (Seed 0 bs)
      nDecoded = FL.fold FL.length frame
  in if null errs
     then Right $ SFrame frame
     else Left
          $ "Decoding errors (sframeFromByteString): "
          <> show errs <> " after decoding " <> show nDecoded <> " of "
          <> show n <> "."
#elif MIN_VERSION_streamly(0,8,0)
sframeFromByteString :: forall c rs. (FS.RecVec rs, V.RMap rs, c Word64, c (F.Rec SElField rs))
                     => (forall a. c a => BS.ByteString -> Either Text (a, BS.ByteString)) -> Word64 -> BS.ByteString -> Either Text (SFrameRec rs)
sframeFromByteString decodeOne n bs =
  let fToFrame :: forall s. Streamly.Fold.Fold (ST.ST s) (Either Text (F.Rec F.ElField rs)) (F.FrameRec rs)
      fToFrame = Streamly.Fold.lmap rightToMaybe $ Streamly.Fold.catMaybes (FS.inCoreAoS_F @rs @StreamType)
      fToErrs :: forall s. Streamly.Fold.Fold (ST.ST s) (Either Text (F.Rec F.ElField rs)) [Text]
      fToErrs = Streamly.Fold.lmap leftToMaybe $ Streamly.Fold.catMaybes Streamly.Fold.toList
      fld :: forall s. Streamly.Fold.Fold (ST.ST s) (Either Text (F.Rec F.ElField rs)) (F.FrameRec rs, [Text])
      fld = Streamly.Tee.toFold $ (,) <$> Streamly.Tee.Tee fToFrame <*> Streamly.Tee.Tee fToErrs
      (frame, errs) = ST.runST $ Streamly.Unfold.fold fld (uDeserialize @c decodeOne n) (Seed 0 bs)
      nDecoded = FL.fold FL.length frame
  in if null errs
     then Right $ SFrame frame
     else Left
          $ "Decoding errors (sframeFromByteString): "
          <> show errs <> " after decoding " <> show nDecoded <> " of "
          <> show n <> "."
#else
sframeFromByteString :: forall c rs. (FS.RecVec rs, V.RMap rs, c Word64, c (F.Rec SElField rs))
                     => (forall a. c a => BS.ByteString -> Either Text (a, BS.ByteString)) -> Word64 -> BS.ByteString -> Either Text (SFrameRec rs)
sframeFromByteString decodeOne n bs =
  let fToFrame :: forall s. Streamly.Fold.Fold (ST.ST s) (Either Text (F.Rec F.ElField rs)) (F.FrameRec rs)
      fToFrame = Streamly.Fold.lmap rightToMaybe $ Streamly.Fold.lcatMaybes FS.inCoreAoS_F
      fToErrs :: forall s. Streamly.Fold.Fold (ST.ST s) (Either Text (F.Rec F.ElField rs)) [Text]
      fToErrs = Streamly.Fold.lmap leftToMaybe $ Streamly.Fold.lcatMaybes Streamly.Fold.toList
      fld :: forall s. Streamly.Fold.Fold (ST.ST s) (Either Text (F.Rec F.ElField rs)) (F.FrameRec rs, [Text])
      fld = (,) <$> fToFrame <*> fToErrs
      (frame, errs) = ST.runST $ Streamly.Unfold.fold (uDeserialize @c decodeOne n) fld (Seed 0 bs)
      nDecoded = FL.fold FL.length frame
  in if null errs
     then Right $ SFrame frame
     else Left
          $ "Decoding errors (sframeFromByteString): "
          <> show errs <> " after decoding " <> show nDecoded <> " of "
          <> show n <> "."
#endif

{-# INLINEABLE sframeFromByteString #-}

{-
newtype SFrameA a = SFrameA { unSFrameA :: F.Frame a } deriving (Eq)
type SFrameRecA rs = SFrameA (F.Record rs)
deriving instance Show (F.Frame a) => Show (SFrameA a)

extraBits :: Flat.NumBits -> Flat.NumBits
extraBits n = let s = n `mod` 8 in if s == 0 then 0 else 8 - s

makeFiller :: Flat.NumBits -> Flat.Filler
makeFiller 0 = error "makeFiller called with 0"
makeFiller 1 = Flat.FillerEnd
makeFiller n = Flat.FillerBit $ makeFiller (n-1)

instance (V.RMap rs, FI.RecVec rs, RecFlat rs, e ~ FlatD.DecodeException) => Flat.Flat (SFrameRecA rs) where
  size l n = let rawBits = Flat.size (sFrameRecToList unSFrameA l) 0 in n + rawBits + extraBits rawBits
  encode l =
    let fl = sFrameRecToList unSFrameA l
        eb = extraBits $  Flat.size fl 0
        encodedFiller = if eb == 0 then mempty else Flat.encode (makeFiller eb)
    in (Flat.encode fl) <> encodedFiller
  decode = do
    sf <- SFrameA <$> FlatS.getUsingStreamly (Streamly.Fold.lmap fromS FS.inCoreAoS_F) Flat.decode
    _::Flat.Filler <- Flat.decode -- decode the filler
    return sf


newtype SFrameB a = SFrameB { unSFrameB :: F.Frame a } deriving (Eq)
type SFrameRecB rs = SFrameB (F.Record rs)
deriving instance Show (F.Frame a) => Show (SFrameB a)

instance (V.RMap rs, FI.RecVec rs, RecFlat rs, e ~ FlatD.DecodeException) => Flat.Flat (SFrameRecB rs) where
  size l = Flat.size (Flat.preAligned $ sFrameRecToList unSFrameB l)
  encode l = Flat.encode (Flat.preAligned $ sFrameRecToList unSFrameB l)
  decode = SFrameB <$> Flat.preAlignedDecoder (FlatS.getUsingStreamly (Streamly.Fold.lmap fromS FS.inCoreAoS_F) Flat.decode)

newtype SFrameC a = SFrameC { unSFrameC :: F.Frame a } deriving (Eq)
type SFrameRecC rs = SFrameC (F.Record rs)
deriving instance Show (F.Frame a) => Show (SFrameC a)


instance (V.RMap rs, FI.RecVec rs, RecFlat rs, e ~ FlatD.DecodeException) => Flat.Flat (SFrameRecC rs) where
  size l = Flat.size (Flat.postAligned $ sFrameRecToList unSFrameC l)
  encode l = Flat.encode (Flat.postAligned $ sFrameRecToList unSFrameC l)
  decode = SFrameC <$> Flat.postAlignedDecoder (FlatS.getUsingStreamly (Streamly.Fold.lmap fromS FS.inCoreAoS_F) Flat.decode)

-}

{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE BangPatterns        #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE CPP             #-}
{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DerivingVia         #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -fwarn-incomplete-patterns #-}
module Flat.Stream
  (
    -- * Types
    UnfolderIO
  , ElementOrS (..)
  , UnfoldS (..)
    -- * decoders
  , lazyDecodeListWith
  , lazyDecodeArrayWith
    -- * get transformers
  , getViaListTFoldM
  , getViaListTFold
  , getViaListTFold'
  , getListViaStreamlyFold
  , getArrayViaStreamlyFold
  )
where

import qualified Control.Foldl as FL

import qualified Flat.Decoder as Flat
import qualified Flat.Decoder.Types as FlatD

import qualified Foreign
import qualified ListT

import qualified Streamly.Internal.Data.Fold as Streamly.Fold
import qualified Streamly.Internal.Data.Unfold as Streamly.Unfold

#if MIN_VERSION_streamly(0,9,0)
--import Streamly.Internal.Data.Unfold as Streamly.Unfold
import qualified Streamly.Data.Fold as Streamly.Tee
#elif MIN_VERSION_streamly(0,8,0)
import qualified Streamly.Internal.Data.Unfold.Type as Streamly.Unfold
import qualified Streamly.Data.Fold.Tee as Streamly.Tee
#else
import qualified Streamly.Internal.Data.Unfold.Type as Streamly.Unfold
import qualified Streamly.Internal.Data.Stream.StreamD.Type as Streamly.Unfold (Step(..))
#endif

import qualified Data.ByteString as BS
import qualified Data.ByteString.Internal as BS


lazyDecodeListWith :: Flat.Get a -> BS.ByteString -> IO (ListT.ListT IO a)
lazyDecodeListWith getA (BS.PS basePtr offset len) =
  Foreign.withForeignPtr basePtr $ \base -> do
    let !endPtr = base `Foreign.plusPtr` len
        !currPtr = base `Foreign.plusPtr` offset
        s0 = FlatD.S currPtr 0
    unfoldLazyList ListT.unfoldM endPtr getA s0
{-# INLINEABLE lazyDecodeListWith #-}

-- | Unfold something encoded via [] to a lazy list without keeping the final state.
-- Useful only used by itself, not as part of another decoder.
unfoldLazyList :: UnfolderIO FlatD.S a g
               -> Foreign.Ptr Word8
               -> Flat.Get a
               -> FlatD.S
               -> IO (g a)
unfoldLazyList unfoldM endPtr getA s0 =
  unfoldFlat unfoldM (bareListUnfoldStep endPtr getA) return s0
{-# INLINEABLE unfoldLazyList #-}


lazyDecodeArrayWith :: Flat.Get a -> BS.ByteString -> IO (ListT.ListT IO a)
lazyDecodeArrayWith getA (BS.PS basePtr offset len) =
  Foreign.withForeignPtr basePtr $ \base -> do
    let !endPtr = base `Foreign.plusPtr` len
        !currPtr = base `Foreign.plusPtr` offset
        !s0 = FlatD.S currPtr 0
    unfoldLazyArray ListT.unfoldM endPtr getA s0
{-# INLINEABLE lazyDecodeArrayWith #-}

-- | Unfold something encoded via Array to a lazy list without keeping the final state.
-- Useful only used by itself, not as part of another decoder.
unfoldLazyArray :: UnfolderIO SN a g
               -> Foreign.Ptr Word8
               -> Flat.Get a
               -> FlatD.S
               -> IO (g a)
unfoldLazyArray unfoldM endPtr getA s0 =
  unfoldFlat unfoldM (bareArrayUnfoldStep endPtr getA) (\fs -> return $ SN fs 0) s0
{-# INLINEABLE unfoldLazyArray #-}



unfoldFlat :: UnfolderIO us a g
           -> UnfoldStepIO us a
           -> (FlatD.S -> IO us)
           -> FlatD.S
           -> IO (g a)
unfoldFlat unfoldM uStep uInject s0 = uInject s0 >>= return . unfoldM uStep
{-# INLINEABLE unfoldFlat #-}

-- | Types to simplify type signatures and monorphize to IO
type UnfoldStepIO us ua = us -> IO (Maybe (ua, us))
type UnfolderIO us ua g = UnfoldStepIO us ua -> us -> g ua

-- | State for decoder unfolds.  Either keep decoding from the given state or stop.
-- We can't just use `FlatD.S` itself since we need to emit the final `FlatD.S` from the unfold
-- *after* the last element has been decoded.
data UnfoldS a = Go a | Stop

-- | Isomorphic to `Either a FlatD.S` but made specific here for readability.
-- We make this strict in case someone uses one of these unfolds to fold to a list or other container which is
-- lazy in the leaves.
data ElementOrS a = Element !a | S !FlatD.S deriving (Functor)

-- | Retrieve the element from an `ElementOrS`.
mElement :: ElementOrS a -> Maybe a
mElement (Element a) = Just a
mElement (S _) = Nothing

-- | Utility for constructing folds to extract the final state in a stream of `ElementOrS`
getFinalS :: Foreign.Ptr Word8 -> FlatD.S -> Maybe (ElementOrS a) -> IO FlatD.S
getFinalS endPtr s0 mE = case mE of
  Nothing -> FlatD.badEncoding endPtr s0 "Empty stream after unfolding (via unfoldLazyResultList) in getUsingLazyList (exception reporting initial S)"
  Just (Element _) -> FlatD.badEncoding endPtr s0 "Last item in stream from unfoldLazyResultList is an element but shoud be an S (exception reporting initial S)"
  Just (S s') -> return s'

{-
This encosing uses Bools between each list entry.  If the Bool is True, an entry follows, if False then the list is done.
-}
listUnfoldStep ::
  (us -> Either (Maybe (ua, us)) FlatD.S)
  -> (a -> FlatD.S -> Maybe (ua, us))
  -> (FlatD.S -> Maybe (ua, us))
  -> Foreign.Ptr Word8
  -> FlatD.Get a
  -> UnfoldStepIO us ua
listUnfoldStep mPre ifA ifEnd endPtr getA us =
  let getNextA fs = do
        FlatD.GetResult !fs' !a <- FlatD.runGet getA endPtr fs
        pure $ ifA a fs'
  in case mPre us of
    Left x -> return x
    Right fs -> do
      FlatD.GetResult !fs' !b <- FlatD.runGet Flat.dBool endPtr fs
      if b then getNextA fs' else return $ ifEnd fs'
{-# INLINEABLE listUnfoldStep #-}

bareListUnfoldStep :: Foreign.Ptr Word8 -> Flat.Get a -> UnfoldStepIO FlatD.S a
bareListUnfoldStep = listUnfoldStep pre ifA ifEnd where
  pre !x = Right x
  ifA !a !fs = Just (a, fs)
  ifEnd _ = Nothing
{-# INLINEABLE bareListUnfoldStep #-}

withFinalStateListUnfoldStep :: Foreign.Ptr Word8
                             -> Flat.Get a
                             -> UnfoldStepIO (UnfoldS FlatD.S) (ElementOrS a)
withFinalStateListUnfoldStep = listUnfoldStep pre ifA ifEnd where
  pre x = case x of
    Stop -> Left Nothing
    Go s -> Right s
  ifA !a !fs = Just (Element a, Go fs)
  ifEnd !fs = Just (S fs, Stop)
{-# INLINEABLE withFinalStateListUnfoldStep #-}

-- | Given an unfolding function for a stream type, unfold something which has been encoded as [a] to a
-- stream of `ElementOrS`.
unfoldLazyResultList :: UnfolderIO (UnfoldS FlatD.S) (ElementOrS a) g
                     -> Foreign.Ptr Word8
                     -> Flat.Get a
                     -> FlatD.S
                     -> IO (g (ElementOrS a))
unfoldLazyResultList unfoldM endPtr getA s0 =
  unfoldFlat unfoldM (withFinalStateListUnfoldStep endPtr getA) (return . Go) s0
{-# INLINEABLE unfoldLazyResultList #-}

-- | Given a monadic fold (from the foldl package), lazily unfold, and then strictly fold,
-- something decoded as @[a]@ into a @b@
getViaListTFoldM :: FL.FoldM IO a b -> Flat.Get a -> Flat.Get b
getViaListTFoldM fB getA = FlatD.Get $ \endPtr s -> do
  let fldS = postMapM (getFinalS endPtr s) $ FL.generalize FL.last
      fldB =  prefilterM' (return . mElement) fB
      fldGR = FlatD.GetResult <$> fldS <*> fldB
  stream <- unfoldLazyResultList ListT.unfoldM endPtr getA s
  ListT.applyFoldM fldGR stream
{-# INLINEABLE getViaListTFoldM #-}

-- | Given a monadic folding step and initial @b@, lazily unfold, and then strictly fold,
-- something decoded as @[a]@ into a @b@
getViaListTFold :: (b -> a -> IO b) -> IO b -> Flat.Get a -> Flat.Get b
getViaListTFold step initM = getViaListTFoldM (FL.FoldM step initM return)
{-# INLINEABLE getViaListTFold #-}

-- | Given a monadic folding step, initial @x@, and monadic @(x -> m b)@ lazily unfold,
-- and then strictly fold, something decoded as @[a]@ into a @b@
getViaListTFold' :: (x -> a -> IO x) -> IO x -> (x -> IO b) -> Flat.Get a -> Flat.Get b
getViaListTFold' step initM extract = getViaListTFoldM (FL.FoldM step initM extract)
{-# INLINEABLE getViaListTFold' #-}

{-
This encoding uses groups of up to 255.  Each is preceded by a Word8 indicating how many follow.
When the Word8 is 0, none remain.
-}
data SN = SN !FlatD.S !Word8

arrayUnfoldStep ::
  (us -> Either (Maybe (ua, us)) SN)
  -> (a -> SN -> Maybe (ua, us))
  -> (FlatD.S -> Maybe (ua, us))
  -> Foreign.Ptr Word8
  -> FlatD.Get a
  -> UnfoldStepIO us ua
arrayUnfoldStep pre ifA ifEnd endPtr getA us = do
  let getNextA !fs !n = do
        FlatD.GetResult !fs' !a <- FlatD.runGet getA endPtr fs
        return $ ifA a (SN fs' (n-1))
      handle0 fs = do
         FlatD.GetResult !fs' !tag <- FlatD.runGet Flat.dWord8 endPtr fs
         if tag == 0 then return (ifEnd fs') else getNextA fs' tag
  case pre us of
    Left !x -> return x
    Right (SN !fs !n) -> if n == 0 then handle0 fs else getNextA fs n

bareArrayUnfoldStep :: Foreign.Ptr Word8 -> Flat.Get a -> UnfoldStepIO SN a
bareArrayUnfoldStep = arrayUnfoldStep pre ifA ifEnd where
  pre !x = Right x
  ifA !a !sn = Just (a, sn)
  ifEnd _ = Nothing
{-# INLINE bareArrayUnfoldStep #-}

withFinalStateArrayUnfoldStep :: Foreign.Ptr Word8 -> Flat.Get a -> UnfoldStepIO (UnfoldS SN) (ElementOrS a)
withFinalStateArrayUnfoldStep = arrayUnfoldStep pre ifA ifEnd where
  pre !x = case x of
    Stop -> Left Nothing
    Go s -> Right s
  ifA !a !sn = Just (Element a, Go sn)
  ifEnd !fs = Just (S fs, Stop)
{-# INLINEABLE withFinalStateArrayUnfoldStep #-}

maybeToStep :: Maybe (a, s) -> Streamly.Unfold.Step s a
maybeToStep Nothing = Streamly.Unfold.Stop
maybeToStep (Just (a, s)) = Streamly.Unfold.Yield a s
{-# INLINE maybeToStep #-}

streamlyUnfold' :: UnfoldStepIO (UnfoldS s) ua
                -> (s -> IO (UnfoldS s))
                -> Streamly.Unfold.Unfold IO s ua
streamlyUnfold' mStep inject = Streamly.Unfold.Unfold (fmap maybeToStep . mStep) inject
{-# INLINE streamlyUnfold' #-}

streamlyUnfold :: (Foreign.Ptr Word8 -> Flat.Get a -> UnfoldStepIO (UnfoldS s) ua)
               -> (s -> IO (UnfoldS s))
               -> Foreign.Ptr Word8
               -> Flat.Get a
               -> Streamly.Unfold.Unfold IO s ua
streamlyUnfold !us !inject !endPtr !getA = streamlyUnfold' (us endPtr getA) inject
{-# INLINE streamlyUnfold #-}

streamlyListUnfold :: Foreign.Ptr Word8 -> Flat.Get a -> Streamly.Unfold.Unfold IO FlatD.S (ElementOrS a)
streamlyListUnfold = streamlyUnfold withFinalStateListUnfoldStep (return . Go)
{-# INLINEABLE streamlyListUnfold #-}

-- | Given a monadic fold for a Streamly stream, lazily unfold,
-- and then strictly fold, something encoded as [a] into a @b@
getListViaStreamlyFold :: Streamly.Fold.Fold IO a b -> Flat.Get a -> Flat.Get b
getListViaStreamlyFold fB getA = FlatD.Get $ \endPtr s0 -> do
#if MIN_VERSION_streamly(0,8,0)
  let rmapM = Streamly.Fold.rmapM
#else
  let rmapM = Streamly.Fold.mapM
#endif
  let fldS = Streamly.Tee.Tee $ rmapM (getFinalS endPtr s0) $ Streamly.Fold.latest
      fldB = Streamly.Tee.Tee $ Streamly.Fold.lmap mElement $ Streamly.Fold.catMaybes $ fB
      fldGR = FlatD.GetResult <$> fldS <*> fldB
  Streamly.Unfold.fold (Streamly.Tee.unTee fldGR) (streamlyListUnfold endPtr getA)  s0
{-# INLINEABLE getListViaStreamlyFold #-}

streamlyArrayUnfold :: Foreign.Ptr Word8 -> Flat.Get a -> Streamly.Unfold.Unfold IO SN (ElementOrS a)
streamlyArrayUnfold = streamlyUnfold withFinalStateArrayUnfoldStep (return . Go)
{-# INLINEABLE streamlyArrayUnfold #-}

-- | Given a monadic fold for a Streamly stream, lazily unfold,
-- and then strictly fold, something encoded as [a] into a @b@
getArrayViaStreamlyFold :: Streamly.Fold.Fold IO a b -> Flat.Get a -> Flat.Get b
getArrayViaStreamlyFold fB getA = FlatD.Get $ \endPtr s0 -> do
#if MIN_VERSION_streamly(0,8,0)
  let rmapM = Streamly.Fold.rmapM
#else
  let rmapM = Streamly.Fold.mapM
#endif
  let fldS = Streamly.Tee.Tee $ rmapM (getFinalS endPtr s0) $ Streamly.Fold.latest
      fldB = Streamly.Tee.Tee $ Streamly.Fold.lmap mElement $ Streamly.Fold.catMaybes $ fB
#if MIN_VERSION_streamly(0,8,0)
      fldGR = FlatD.GetResult <$> fldS <*> fldB
#else
      fldGR = FlatD.GetResult <$> fldS <*> fldB
#endif
  Streamly.Unfold.fold (Streamly.Tee.unTee fldGR) (streamlyArrayUnfold endPtr getA) (SN s0 0)
{-# INLINEABLE getArrayViaStreamlyFold #-}

-- Some things which should be in foldl but aren't
postMapM :: Monad m => (a -> m b) -> FL.FoldM m x a -> FL.FoldM m x b
postMapM f (FL.FoldM step begin done) = FL.FoldM step begin done'
  where done' x = done x >>= f
{-# INLINABLE postMapM #-}

prefilterM' :: Monad m => (a -> m (Maybe b)) -> FL.FoldM m b r -> FL.FoldM m a r
prefilterM' f (FL.FoldM step begin done) = FL.FoldM step' begin done
  where
    step' !x !a = do
      mUse <- f a
      case mUse of
        Nothing -> return x
        Just !b -> step x b
{-# INLINABLE prefilterM' #-}

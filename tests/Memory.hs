{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE CPP          #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -O0 #-}
module Main where

import MemoryPaths

import qualified Control.Foldl as FL
import Control.Exception (evaluate)
import qualified Data.ByteString as BS
import qualified Data.Text as T
import qualified Data.Serialize                as S
import qualified Data.Vector as Vec
import qualified Flat
import qualified Frames as F
import qualified Frames.Streamly.InCore as FI
import qualified Frames.Streamly.CSV as FStreamly
import qualified Frames.Streamly.TH as FStreamly
import qualified Frames.Streamly.InCore as FStreamly
import qualified Frames.Streamly.Streaming.Streamly as FStreamly
import qualified Frames.Serialize as FS
import qualified Frames.CSV                     as Frames
#if MIN_VERSION_streamly(0,9,0)
import qualified Streamly.Data.Fold            as Streamly.Fold
import qualified Streamly.Data.Stream          as Streamly
--import qualified Streamly              as Streamly
import qualified Streamly.Internal.FileSystem.File
                                               as Streamly.File
--import qualified Streamly.Memory.Array as Streamly.Array
#else
import qualified Streamly.Data.Fold            as Streamly.Fold
import qualified Streamly.Prelude              as Streamly
import qualified Streamly              as Streamly
import qualified Streamly.Internal.FileSystem.File
                                               as Streamly.File
import qualified Streamly.Memory.Array as Streamly.Array
#endif
import qualified Streamly.External.ByteString as SB

import qualified Test.Hspec as Test
import qualified Test.Hspec.QuickCheck as Test
import qualified Test.QuickCheck as Test
import Test.QuickCheck.Instances ()


import Data.Vinyl.Core ()
import qualified Data.Vinyl as V
import System.TimeIt as TimeIt

F.tableTypes' pumsACS1YrRowGen

data TestA f = TestA (f PUMS_Raw) Text (f PUMS_Raw) Text deriving Generic

instance Flat.Flat (TestA FS.SFrame)

instance Flat.Flat (TestA F.Frame) where
  size (TestA f1 t1 f2 t2) n = Flat.size (FS.SFrame f1) . Flat.size t1 . Flat.size (FS.SFrame f2) $ Flat.size t2 n
  encode (TestA f1 t1 f2 t2) = Flat.encode (FS.SFrame f1) <> Flat.encode t1 <> Flat.encode (FS.SFrame f2) <> Flat.encode t2
  decode = TestA <$> (FS.unSFrame <$> Flat.decode) <*> Flat.decode <*> (FS.unSFrame <$> Flat.decode) <*> Flat.decode


data TestB f = TestB (f PUMS_Raw) (f PUMS_Raw) deriving Generic

instance Flat.Flat (TestB FS.SFrame)

instance Flat.Flat (TestB F.Frame) where
  size (TestB f1 f2) n = Flat.size (Flat.preAligned $ FS.SFrame f1) $ Flat.size (Flat.preAligned $ FS.SFrame f2) n
  encode (TestB f1 f2) = Flat.encode (FS.SFrame f1) <> Flat.encode (FS.SFrame f2)
  decode = TestB <$> (FS.unSFrame <$> Flat.decode) <*> (FS.unSFrame <$> Flat.decode)

data TestC f = TestC !(f PUMS_Raw) !(f PUMS_Raw) !(Map Text Int) deriving (Generic)
instance (Eq (f PUMS_Raw)) => Eq (TestC f)

instance Flat.Flat (TestC F.Frame) where
  size (TestC f1 f2 m) = Flat.size (FS.SFrame f1, FS.SFrame f2, m)
  encode (TestC f1 f2 m) = Flat.encode (FS.SFrame f1, FS.SFrame f2, m)
  decode = (\(sf1, sf2, m) -> TestC (FS.unSFrame sf1) (FS.unSFrame sf2) m) <$> Flat.decode

instance Flat.Flat (TestC FS.SFrame)

data TestEnum = E1 | E2 | E3 deriving (Generic, Eq, Ord, Enum, Show, Flat.Flat)
type instance FI.VectorFor TestEnum = Vec.Vector
instance Test.Arbitrary TestEnum where
  arbitrary = Test.arbitrary @Int >>= \n -> return $ toEnum (n `mod` 3)

type ColA = "A" F.:-> Text
type ColB = "B" F.:-> Int
type ColC = "C" F.:-> Double
type ColD = "D" F.:-> TestEnum

type TestRow = [ColA, ColB, ColC, ColD]

type RowList = [F.Record TestRow]
--type RowFrame = F.Frame TestRow


instance Test.Arbitrary (F.Record TestRow) where
  arbitrary = (\t i d e -> t F.&: i F.&: d F.&: e F.&: V.RNil) <$> Test.arbitrary <*> Test.arbitrary <*> Test.arbitrary <*> Test.arbitrary

main :: IO ()
main = do
  let pumsCSV = "test_data/acs100k.csv"
  putTextLn "Testing File.toBytes..."
  let rawBytesS =  Streamly.unfold Streamly.File.chunkReader pumsCSV
  rawBytes <-  Streamly.fold Streamly.Fold.length rawBytesS
  putTextLn $ "raw PUMS data has " <> show rawBytes <> " bytes."
  putTextLn "Testing readTable..."
  let sPUMSRawRows :: FStreamly.DefaultStream IO PUMS_Raw
        = FStreamly.readTableOpt @_ @FStreamly.DefaultStream @IO FStreamly.defaultParser pumsCSV
  iRows <-  Streamly.fold Streamly.Fold.length $ FStreamly.stream sPUMSRawRows
  putTextLn $ "raw PUMS data has " <> (T.pack $ show iRows) <> " rows."
  putTextLn "Testing Frames.Streamly.inCoreAoS:"
  fPums' <- FStreamly.inCoreAoS sPUMSRawRows
  let fPums2 = fPums' <> fPums'
      fPums4 = fPums2 <> fPums2
      fPums8 = fPums4 <> fPums4
      fPums16 = fPums8 <> fPums8
      fPums32 = fPums16 <> fPums16
      fPums64 = fPums32 <> fPums32
      fPums = fPums64
  putTextLn $ "raw PUMS frame has " <> show (F.frameLength fPums) <> " rows."

  -- serialize
  putTextLn $ "\nTesting serialization"
  -- cereal  putTextLn $ "Cereal encode"
  let sBytesCereal = S.encode $ FS.SFrame fPums
  TimeIt.timeIt $ putTextLn $ "pums frame encodes to " <> show (BS.length sBytesCereal) <> " bytes."
  putTextLn $ "Cereal decode"
  TimeIt.timeIt $ case S.decode @(FS.SFrame PUMS_Raw) sBytesCereal of
    Left msg -> putTextLn $ "Error decoding: " <> toText msg
    Right sf -> putTextLn $ "Decoded to " <> show (F.frameLength $ FS.unSFrame sf) <> " rows."

  putTextLn $ "\nFlat (list encoder/decoder)"
  putTextLn $ "Frames.frameLength=" <> show (F.frameLength fPums)
  putTextLn $ "Flat computed size: " <> show (Flat.size (FS.SFrame fPums) 0)
  let sBytesFlat = Flat.flat $ FS.SFrame fPums
  TimeIt.timeIt $ putTextLn $ "pums frame encodes to " <> show (BS.length sBytesFlat) <> " bytes."
  TimeIt.timeIt $ case Flat.unflat @(FS.SFrame PUMS_Raw) sBytesFlat of
      Left msg -> putTextLn $ "Error decoding: " <> show msg
      Right sf -> putTextLn $ "Decoded to " <> show (F.frameLength $ FS.unSFrame sf) <> " rows."


  putTextLn $ "\nFlat (array encoder/decoder)"
  putTextLn $ "Frames.frameLength=" <> show (F.frameLength fPums)
  putTextLn $ "Flat computed size: " <> show (Flat.size (FS.SFrameA fPums) 0)
  let sBytesFlat = Flat.flat $ FS.SFrameA fPums
  TimeIt.timeIt $ putTextLn $ "pums frame encodes to " <> show (BS.length sBytesFlat) <> " bytes."
  TimeIt.timeIt $ case Flat.unflat @(FS.SFrameA PUMS_Raw) sBytesFlat of
      Left msg -> putTextLn $ "Error decoding: " <> show msg
      Right sf -> putTextLn $ "Decoded to " <> show (F.frameLength $ FS.unSFrameA sf) <> " rows."


  -- property testing
  Test.hspec $ do
    Test.describe "Flat roundTrip" $ do
      Test.context "When used with [Int]" $ do
        Test.it "should be true" $ Test.property @([Int] -> IO ()) $
          \b -> roundTripProp show b >>=  (`Test.shouldBe` True)

    let showTuple unS (sf1, m1, sf2, m2) = "("
                                           <> show (FL.fold FL.list (unS sf1))
                                           <> ","
                                           <> show m1
                                           <> ","
                                           <> show (FL.fold FL.list (unS sf2))
                                           <> ","
                                           <> show m2
                                           <> ")"

    Test.describe "Flat (list encode/decode) roundTrip" $ do
      Test.context "when used with (SFrame , Map Text Int, SFrame, Map Int Text)" $ do
        Test.it "should be true" $ Test.property @(Map Text Int -> Map Int Text -> IO ()) $
          \m1 m2 -> roundTripProp (showTuple FS.unSFrame) (FS.SFrame fPums, m1, FS.SFrame fPums, m2) >>= (`Test.shouldBe` True)

    Test.describe "Flat (array encode/decode) roundTrip" $ do
      Test.context "when used with (SFrameA , Map Text Int, SFrameA, Map Int Text)" $ do
        Test.it "should be true" $ Test.property @(Map Text Int -> Map Int Text -> IO ()) $
          \m1 m2 -> roundTripProp (showTuple FS.unSFrameA) (FS.SFrameA fPums, m1, FS.SFrameA fPums, m2) >>= (`Test.shouldBe` True)

  return ()

testLoop :: Flat.Flat a => a -> Text -> IO (Either Text a)
testLoop a fp = do
  encodeAndWrite a fp
  readAndDecode fp

decodeEncodingProp :: forall a.Flat.Flat a => a -> IO Bool
decodeEncodingProp x = case Flat.unflat (Flat.flat x) of
                         Left err -> putTextLn (show err) >> return False
                         Right (_::a) -> return True

decodeEncodingFileProp :: forall a.Flat.Flat a => Text -> a -> IO Bool
decodeEncodingFileProp fp a = do
  encodeAndWrite a fp
  resE <- readAndDecode fp
  case resE of
    Left _ -> return False
    Right (_::a) -> return True


roundTripProp :: (Flat.Flat a, Eq a) => (a -> Text) -> a -> IO Bool
roundTripProp showA !x =
  case Flat.unflat (Flat.flat x) of
    Left err -> putTextLn (show err) >> return False
    Right a -> if (x == a)
                then return True
                else (putTextLn $ showA x <> " /= " <> showA a) >> return False

roundTripFileProp :: (Flat.Flat a, Eq a) => a -> Text -> IO Bool
roundTripFileProp !a fp = do
  encodeAndWrite a fp
  resE <- readAndDecode fp
  case resE of
    Left err -> putTextLn err >> return False
    Right a' -> return $ a == a'

encodeAndWrite :: Flat.Flat a => a -> Text -> IO ()
encodeAndWrite x fp = do
  let flatBits = Flat.getSize x
  putTextLn $ "Flat computed size: " <> show flatBits <> " bits = " <> show (flatBits `div` 8) <> " bytes, "
    <> "with " <> show (flatBits `rem` 8) <> " bits left over."
  let flatBS = Flat.flat x
  putTextLn $ "Flat encoded bytestring size: " <> show (BS.length flatBS) <> " bytes.  Writing file..."
  BS.writeFile (toString fp) flatBS

readAndDecode :: Flat.Flat a => Text -> IO (Either Text a)
readAndDecode fp = do
  putTextLn $ "Reading \"" <> fp <> "\"..."
  flatBS <- BS.readFile (toString fp)
  putTextLn $ "read ByteString size: " <> show (BS.length flatBS) <> " bytes.  Decoding..."
  let eA = Flat.unflat flatBS
  case eA of
    Left err -> (putTextLn $ "Error decoding: " <> show err) >> return (Left $ show err)
    Right a -> do
      let flatBits = Flat.getSize a
      putTextLn $ "Flat computed size: " <> show flatBits <> " bits = " <> show (flatBits `div` 8) <> " bytes, "
        <> "with " <> show (flatBits `rem` 8) <> " bits left over."
      return $ Right a
